package com.sornambigai.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "bill_sequence")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class BillSequenceDto {

	@Column(name = "bill_sequence")
	@Id
	@GeneratedValue
	private long billSequence;
}
