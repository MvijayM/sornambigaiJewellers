package com.sornambigai.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "bill_header")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BillHeaderDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -107698799467431118L;

	@Column(name = "bill_header_id")
	@Id
	@GeneratedValue
	private long billHeaderId;

	@Column(name = "bill_id")
	private String billId;

	@Column(name = "bill_registered_date")
	private Timestamp billRegisteredDate;

	@Column(name = "bill_status")
	private int billStatus;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "cashier_name")
	private String cashierName;

	@OneToMany(targetEntity = BillDetailDto.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "bill_id", referencedColumnName = "bill_header_id")
	private List<BillDetailDto> items;

	@Column(name = "total_bill_amount")
	private BigDecimal totalBillAmount;

}
