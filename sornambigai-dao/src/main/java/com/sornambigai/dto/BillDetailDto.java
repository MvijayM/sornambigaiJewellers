package com.sornambigai.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name ="bill_detail")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BillDetailDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8226647657960170041L;

	@Id
	@GeneratedValue
	@Column(name="bill_detail_id")
	private int billDetailId;
	
	@Column(name = "item_category_id")
	private String itemCategoryId; 
	
	@Column(name="item_id")
	private long itemId;
	
	@Column(name="item_name")
	private String itemName;	
	
	@Column(name="price_per_gram_on_purchase")
	private BigDecimal pricePerGramOnPurchase;
	
	@Column(name="grams_purchased")
	private BigDecimal gramsPurchased;
	
	@Column(name="making_charge")
	private BigDecimal makingCharge;
	
	@Column(name="wastage")
	private BigDecimal wastage;
	
	@Column(name="quantity")
	private long quantity;
	
	@Column(name="item_amount")
	private BigDecimal itemAmount;
	
}
