package com.sornambigai.entity;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;

import com.sornambigai.dto.BillDetailDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BillDetailEntity {
	private int billDetailId;

	private String itemCategoryId; 

	private long itemId;

	private String itemName;	

	private BigDecimal pricePerGramOnPurchase;

	private BigDecimal gramsPurchased;
	
	private BigDecimal makingCharge;
	
	private BigDecimal wastage;
	
	private long quantity;
	
	private BigDecimal itemAmount;
	public static BillDetailEntity formEntity(BillDetailDto dto) {
		if(Objects.nonNull(dto)) {
			BillDetailEntity entity = new BillDetailEntity();
			entity.setBillDetailId(dto.getBillDetailId());
			entity.setItemCategoryId(dto.getItemCategoryId());
			entity.setItemId(dto.getItemId());
			entity.setItemName(dto.getItemName());
			entity.setPricePerGramOnPurchase(dto.getPricePerGramOnPurchase());
			entity.setGramsPurchased(dto.getGramsPurchased());
			entity.setMakingCharge(dto.getMakingCharge());
			entity.setWastage(dto.getWastage());
			entity.setQuantity(dto.getQuantity());
			entity.setItemAmount(dto.getItemAmount());
			return entity;
		}
		return null;
	}

	public static BillDetailDto formDto(BillDetailEntity billDetailEntity) {
		if(Objects.nonNull(billDetailEntity)) {
			BillDetailDto billDetailDto = new BillDetailDto();
			billDetailDto.setBillDetailId(billDetailEntity.getBillDetailId());
			billDetailDto.setItemCategoryId(billDetailEntity.getItemCategoryId());
			billDetailDto.setItemId(billDetailEntity.getItemId());
			billDetailDto.setItemName(billDetailEntity.getItemName());
			billDetailDto.setPricePerGramOnPurchase(billDetailEntity.getPricePerGramOnPurchase());
			billDetailDto.setGramsPurchased(billDetailEntity.getGramsPurchased());
			billDetailDto.setMakingCharge(billDetailEntity.getMakingCharge());
			billDetailDto.setWastage(billDetailEntity.getWastage());
			billDetailDto.setQuantity(billDetailEntity.getQuantity());
			billDetailDto.setItemAmount(billDetailEntity.getItemAmount());
			return billDetailDto;
		}
		return null;
	}

}
