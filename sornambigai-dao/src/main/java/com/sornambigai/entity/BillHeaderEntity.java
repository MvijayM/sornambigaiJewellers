package com.sornambigai.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.sornambigai.dto.BillHeaderDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BillHeaderEntity {

	private long billHeaderId;

	private String billId;

	private Timestamp billRegisteredDate;

	private BillStatus billStatus;

	private String customerName;

	private String cashierName;

	private List<BillDetailEntity> items;

	private BigDecimal totalBillAmount;

	public static BillHeaderEntity formEntity(BillHeaderDto billHeaderDto) {
		if (Objects.nonNull(billHeaderDto)) {
			BillHeaderEntity entity = new BillHeaderEntity();
			entity.setBillHeaderId(billHeaderDto.getBillHeaderId());
			entity.setBillId(billHeaderDto.getBillId());
			entity.setBillStatus(BillStatus.of(billHeaderDto.getBillStatus()));
			entity.setBillRegisteredDate(billHeaderDto.getBillRegisteredDate());
			entity.setCashierName(billHeaderDto.getCashierName());
			entity.setCustomerName(billHeaderDto.getCustomerName());
			entity.setTotalBillAmount(billHeaderDto.getTotalBillAmount());
			entity.setItems(Objects.nonNull(billHeaderDto.getItems())
					? billHeaderDto.getItems().stream().map(BillDetailEntity::formEntity).collect(Collectors.toList())
					: new ArrayList<>());
			return entity;
		}
		return null;
	}

	public static BillHeaderDto formDto(BillHeaderEntity dto) {
		if (Objects.nonNull(dto)) {
			BillHeaderDto billHeaderEntity = new BillHeaderDto();
			billHeaderEntity.setBillHeaderId(dto.getBillHeaderId());
			billHeaderEntity.setBillId(dto.getBillId());
			billHeaderEntity.setBillStatus(dto.getBillStatus().getStatusKey());
			billHeaderEntity.setCashierName(dto.getCashierName());
			billHeaderEntity.setCustomerName(dto.getCustomerName());
			billHeaderEntity.setBillRegisteredDate(dto.getBillRegisteredDate());
			billHeaderEntity.setTotalBillAmount(dto.getTotalBillAmount());
			billHeaderEntity.setItems(Objects.nonNull(dto.getItems())
					? dto.getItems().stream().map(BillDetailEntity::formDto).collect(Collectors.toList())
					: new ArrayList<>());
			return billHeaderEntity;
		}
		return null;
	}

}
