package com.sornambigai.entity;

import java.util.Objects;

import javax.persistence.Entity;

import com.sornambigai.dto.UsersDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

	private String emailId;

	private String userName;

	private String password;
	
	private UserRole role;

	public static UserEntity formEntity(UsersDto dto) {
		if (Objects.nonNull(dto)) {
			UserEntity userEntity = new UserEntity();
			userEntity.setEmailId(dto.getEmailId());
			userEntity.setUserName(dto.getUserName());
			userEntity.setPassword(dto.getPassword());
			userEntity.setRole(UserRole.valueOf(dto.getRole()));
			return userEntity;
		} else {
			return null;
		}
	}
	
	public static UsersDto formDto(UserEntity userEntity) {
		if (Objects.nonNull(userEntity)) {
			UsersDto userDto = new UsersDto();
			userDto.setEmailId(userEntity.getEmailId());
			userDto.setUserName(userEntity.getUserName());
			userDto.setPassword(userEntity.getPassword());
			userDto.setRole(userEntity.getRole().toString());
			return userDto;
		} else {
			return null;
		}
	}

}
