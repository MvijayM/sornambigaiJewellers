package com.sornambigai.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BillCalculationType {

	STANDARD_GOLD(1),
	KDM_GOLD_916(2),
	STANDARD_SILVER(1),
	SILVER_92_5(3);
	
	private int billCalculationType;

	public static BillCalculationType of(int billCaculationType) {
		BillCalculationType[] billTypes = BillCalculationType.values();
		BillCalculationType billType = null;
		for(int index=0;index< billTypes.length;index++) {
			if(billCaculationType == billTypes[index].getBillCalculationType()) {
				billType = billTypes[index];
			}
		}
		return billType;
	}
}
