package com.sornambigai.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.sornambigai.entity.BillHeaderEntity;
import com.sornambigai.entity.BillStatus;
import com.sornambigai.repositories.BillHeaderRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillHeaderDaoImpl implements BillHeaderDao {

	private final BillHeaderRepository billHeaderRepository;

	@Override
	public BillHeaderEntity getBillByBillId(String billId) {
		return BillHeaderEntity.formEntity(billHeaderRepository.getExistingBill(billId));
	}

	@Override
	public boolean insertBill(BillHeaderEntity billHeaderEntity) {
		try {
			billHeaderRepository.save(BillHeaderEntity.formDto(billHeaderEntity));
			return true;
		} catch (Exception e) {
			log.error("Error while inserting bill", e);
			return false;
		}
	}

	@Override
	public List<BillHeaderEntity> getBillsByStatus(BillStatus status) {
		return billHeaderRepository.getBillsByStatus(status.getStatusKey()).stream().map(BillHeaderEntity::formEntity)
				.collect(Collectors.toList());
	}

	@Override
	public boolean deleteBill(BillHeaderEntity billHeaderEntity) {
		try {
			billHeaderRepository.deleteById(billHeaderEntity.getBillHeaderId());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<BillHeaderEntity> searchBills(String keyWord, long offset) {
		return billHeaderRepository.searchBills(keyWord, offset).stream().map(BillHeaderEntity::formEntity)
				.collect(Collectors.toList());
	}

}
