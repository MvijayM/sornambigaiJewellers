package com.sornambigai.dao;

import java.util.List;

import com.sornambigai.entity.BillHeaderEntity;
import com.sornambigai.entity.BillStatus;

public interface BillHeaderDao {

	List<BillHeaderEntity> getBillsByStatus(BillStatus status);

	BillHeaderEntity getBillByBillId(String string);

	boolean insertBill(BillHeaderEntity billHeaderEntity);

	boolean deleteBill(BillHeaderEntity billHeaderEntity);

	List<BillHeaderEntity> searchBills(String keyWord, long offset);
}
