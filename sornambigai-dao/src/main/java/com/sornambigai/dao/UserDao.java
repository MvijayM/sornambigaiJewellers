package com.sornambigai.dao;

import com.sornambigai.entity.UserEntity;

public interface UserDao {

	UserEntity getUserById(String email);

	void save(UserEntity user);

	boolean checkIFAdmintExist();
	
	boolean updatePassword(String emailId, String password);
}