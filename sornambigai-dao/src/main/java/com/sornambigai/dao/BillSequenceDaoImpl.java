package com.sornambigai.dao;

import org.springframework.beans.factory.annotation.Autowired;

import com.sornambigai.dto.BillSequenceDto;
import com.sornambigai.repositories.BillSequenceRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillSequenceDaoImpl implements BillSequenceDao {

	private final BillSequenceRepository billSequenceRepository;

	@Override
	public long getLatestBillSequence() {
		return billSequenceRepository.count();
	}

	@Override
	public boolean incrementSequence() {
		try {
			billSequenceRepository.save(BillSequenceDto.builder().build());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
