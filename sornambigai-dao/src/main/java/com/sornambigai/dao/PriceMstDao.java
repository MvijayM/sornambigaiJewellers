package com.sornambigai.dao;

import java.util.List;

import com.sornambigai.entity.ItemsEntity;
import com.sornambigai.entity.PriceMstEntity;

public interface PriceMstDao {

	public boolean updatePrice(PriceMstEntity priceEntity);

	public List<PriceMstEntity> getPrices();
}
