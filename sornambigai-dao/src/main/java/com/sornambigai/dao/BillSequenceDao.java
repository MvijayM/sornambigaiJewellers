package com.sornambigai.dao;

public interface BillSequenceDao {

	long getLatestBillSequence();

	boolean incrementSequence();
}
