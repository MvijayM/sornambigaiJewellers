package com.sornambigai.biz.billgeneration.service.billmanagement.report;

import java.time.LocalDate;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillReportModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.FileResponseModel;
import com.sornambigai.entity.BillHeaderEntity;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Slf4j
public class ReportServiceImpl implements ReportService {

	@Override
	public FileResponseModel exportBill(BillHeaderEntity billModel, Map<String, String> itemCategoryNameMap) {
		try {
			List<BillReportModel> billDetails = BillReportModel.toReportModel(billModel.getItems(),
					itemCategoryNameMap);
			JRBeanCollectionDataSource gridDataSource = new JRBeanCollectionDataSource(billDetails);
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			parameterMap.put("itemDetailsParam", gridDataSource);
			parameterMap.put("billId", billModel.getBillId());
			parameterMap.put("billDate",
					Objects.nonNull(billModel.getBillRegisteredDate())
							? billModel.getBillRegisteredDate().toLocalDateTime().toLocalDate()
							: LocalDate.now());
			parameterMap.put("customerName", billModel.getCustomerName());
			parameterMap.put("cashierName", billModel.getCashierName());
			JasperDesign jsDesign = JRXmlLoader.load(getClass().getResourceAsStream("/item_details.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(jsDesign);
			JasperPrint jsPrint = JasperFillManager.fillReport(jasperReport, parameterMap, new JREmptyDataSource());
			return FileResponseModel.builder()
					.fileData(Base64.getEncoder().encodeToString(JasperExportManager.exportReportToPdf(jsPrint)))
					.fileName(billModel.getBillId() + ".pdf").result(true).build();
		} catch (JRException jrException) {
			log.error(jrException + "");
			return FileResponseModel.builder().result(false).build();
		}
	}
}
