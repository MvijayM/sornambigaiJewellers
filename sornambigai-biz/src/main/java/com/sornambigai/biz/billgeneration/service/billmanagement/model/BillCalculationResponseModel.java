package com.sornambigai.biz.billgeneration.service.billmanagement.model;

import java.math.BigDecimal;

import com.sornambigai.entity.BillDetailEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Builder
public class BillCalculationResponseModel {
	private BigDecimal totalAmount;
	private BillDetailEntity itemDetails;
}
