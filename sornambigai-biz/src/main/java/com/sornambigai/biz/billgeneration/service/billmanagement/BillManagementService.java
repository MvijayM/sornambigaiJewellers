package com.sornambigai.biz.billgeneration.service.billmanagement;

import java.util.List;

import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationRequestModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationResponseModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.FileResponseModel;
import com.sornambigai.entity.BillHeaderEntity;
import com.sornambigai.entity.BillStatus;

public interface BillManagementService {

	List<BillHeaderEntity> getBillsByStatus(BillStatus status);

	BillHeaderEntity getBillByBillId(String billId);

	BillHeaderEntity insertBill(BillHeaderEntity billRequestModel);

	BillCalculationResponseModel calculateBillForSingleItem(BillCalculationRequestModel requestModel);

	boolean deleteBill(String billId);

	List<BillHeaderEntity> searchBills(String keyword, long offset);

	FileResponseModel closeAndPrintBill(BillHeaderEntity billRequestModel);

	FileResponseModel printBill(String billId);

}
