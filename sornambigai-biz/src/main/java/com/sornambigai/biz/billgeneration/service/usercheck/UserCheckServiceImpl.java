package com.sornambigai.biz.billgeneration.service.usercheck;

import java.util.Base64;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.sornambigai.biz.billgeneration.service.usercheck.model.ChangePasswordResponseModel;
import com.sornambigai.dao.UserDao;
import com.sornambigai.entity.UserEntity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserCheckServiceImpl implements UserCheckService {

	private final UserDao userDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public boolean check(Map<String, Object> requestMap) {
		UserEntity user = userDao.getUserById(
				new String(Base64.getDecoder().decode(String.valueOf(requestMap.get("adminusername").toString()))));
		return Objects.nonNull(user) && user.getPassword().equals(
				new String(Base64.getDecoder().decode(String.valueOf(requestMap.get("adminpassword").toString()))));
	}

	@Override
	public boolean addUser(UserEntity userEntity) {
		userEntity
				.setPassword(passwordEncoder.encode(new String(Base64.getDecoder().decode(userEntity.getPassword()))));
		userEntity.setEmailId(new String(Base64.getDecoder().decode(userEntity.getEmailId())));
		userEntity.setUserName(new String(Base64.getDecoder().decode(userEntity.getUserName())));
		UserEntity existingUser = userDao.getUserById(userEntity.getEmailId());
		if (Objects.nonNull(existingUser)) {
			return false;
		}
		userDao.save(userEntity);
		return true;
	}

	@Override
	public boolean checkIFAdminExist() {
		return userDao.checkIFAdmintExist();
	}

	@Override
	public ChangePasswordResponseModel changePassword(Map<String, Object> requestModel) {
		String userEmailId = new String(Base64.getDecoder().decode(String.valueOf(requestModel.get("userEmailId"))));
		String newPassword = new String(Base64.getDecoder().decode(String.valueOf(requestModel.get("userPassword"))));
		String adminEmail = new String(Base64.getDecoder().decode(String.valueOf(requestModel.get("adminEmailId"))));
		String adminPassword = new String(
				Base64.getDecoder().decode(String.valueOf(requestModel.get("adminPassword"))));
		UserEntity existingUser = userDao.getUserById(userEmailId);
		UserEntity adminUser = userDao.getUserById(adminEmail);
		if (Objects.nonNull(existingUser)) {
			if (Objects.nonNull(adminUser) && passwordEncoder.matches(adminPassword, adminUser.getPassword())) {
				return userDao.updatePassword(userEmailId, passwordEncoder.encode(newPassword))
						? ChangePasswordResponseModel.builder().result(true).build()
						: ChangePasswordResponseModel.builder().result(false).message("Password Updation Failed.")
								.build();
			} else {
				return ChangePasswordResponseModel.builder().result(false).message("Check Admin Email Id or Password.")
						.build();
			}
		} else {
			return ChangePasswordResponseModel.builder().result(false)
					.message("Password Can be changed Only for Existing User. Try Registering.").build();
		}
	}

}
