package com.sornambigai.biz.billgeneration.Contoller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationRequestModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.report.ReportService;
import com.sornambigai.dto.BillHeaderDto;
import com.sornambigai.entity.BillCalculationType;
import com.sornambigai.entity.ItemCategoryMstEntity;
import com.sornambigai.repositories.BillDetailRepository;
import com.sornambigai.repositories.BillHeaderRepository;
import com.sornambigai.repositories.ItemCategoryMstRepository;

@RestController
@RequestMapping(value = "noAuth/master")
public class MasterController {

	@Autowired
	private ItemCategoryMstRepository itemCategoryRepository;

	@Autowired
	private BillHeaderRepository billHeaderRepository;

	@Autowired
	private BillDetailRepository billDetailRepository;

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/addItemCategory", method = RequestMethod.POST)
	public int addItemCategory(@RequestBody Map<String, Object> requestMap) {
		ItemCategoryMstEntity category = new ItemCategoryMstEntity();
		category.setItemCategoryId(String.valueOf(requestMap.get("categoryId")));
		category.setItemCategoryName(String.valueOf(requestMap.get("categoryName")));
		category.setBillCalculationType(BillCalculationType.valueOf(String.valueOf(requestMap.get("billType"))));
		itemCategoryRepository.save(ItemCategoryMstEntity.formDto(category));
		return 1;
	}

	@RequestMapping(value = "/addBill", method = RequestMethod.POST)
	public BillHeaderDto insertBill(@RequestBody BillCalculationRequestModel requestMap) {
		return null;
	}

//	@RequestMapping(value = "/printBill", method = RequestMethod.GET)
//	public byte[] printBill() throws FileNotFoundException, JRException {
//		return reportService.exportBill(
//				BillHeaderEntity.formEntity(billHeaderRepository.getExistingBill("SORNAMBIGAI_2")),
//				itemCategoryRepository.findAll().stream().map(ItemCategoryMstEntity::formEntity)
//						.collect(Collectors.toList()));
//	}

}
