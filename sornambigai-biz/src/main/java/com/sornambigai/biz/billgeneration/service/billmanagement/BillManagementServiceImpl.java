package com.sornambigai.biz.billgeneration.service.billmanagement;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationRequestModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationResponseModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.FileResponseModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.report.ReportService;
import com.sornambigai.biz.billgeneration.service.priceupdate.PriceUpdateService;
import com.sornambigai.dao.BillDetailDao;
import com.sornambigai.dao.BillHeaderDao;
import com.sornambigai.dao.BillSequenceDao;
import com.sornambigai.dao.ItemCategoryMstDao;
import com.sornambigai.entity.BillCalculationType;
import com.sornambigai.entity.BillDetailEntity;
import com.sornambigai.entity.BillHeaderEntity;
import com.sornambigai.entity.BillStatus;
import com.sornambigai.entity.ItemCategoryMstEntity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillManagementServiceImpl implements BillManagementService {

	private final BillHeaderDao billHeaderDao;

	private final BillDetailDao billDetailDao;

	private final ItemCategoryMstDao itemCategoryDao;

	private final BillSequenceDao billSequenceDao;

	private final ReportService reportService;

	private final PriceUpdateService priceUpdateService;

	@Override
	public List<BillHeaderEntity> getBillsByStatus(BillStatus status) {
		return billHeaderDao.getBillsByStatus(status);
	}

	@Override
	public BillHeaderEntity getBillByBillId(String billId) {
		BillHeaderEntity billHeaderEntity = billHeaderDao.getBillByBillId(billId);
		if (Objects.nonNull(billHeaderEntity)
				&& billHeaderEntity.getBillStatus().getStatusKey() != BillStatus.CLOSED.getStatusKey()) {
			List<BillDetailEntity> itemList = billHeaderEntity.getItems();
			Map<String, BigDecimal> priceMap = priceUpdateService.getPrices().stream()
					.collect(Collectors.toMap(price -> price.getItemCategoryId(), price -> price.getPricePerGram()));
			billHeaderEntity.setItems(itemList.stream().map(item -> {
				item.setPricePerGramOnPurchase(priceMap.get(item.getItemCategoryId()));
				BillCalculationResponseModel calculationReponse = calculateBillForSingleItem(
						BillCalculationRequestModel.builder().billHeader(billHeaderEntity).itemDetails(item).build());
				item = calculationReponse.getItemDetails();
				return item;
			}).collect(Collectors.toList()));
			billHeaderEntity.setTotalBillAmount(billHeaderEntity.getItems().stream().map(item -> item.getItemAmount())
					.reduce(BigDecimal.ZERO, BigDecimal::add));
		}
		return billHeaderEntity;
	}

	@Override
	public BillHeaderEntity insertBill(BillHeaderEntity billHeaderEntity) {
		long currentSequence = billSequenceDao.getLatestBillSequence();
		billHeaderEntity.setBillRegisteredDate(Timestamp.valueOf(LocalDateTime.now()));
		BillHeaderEntity existingBill = billHeaderDao.getBillByBillId(billHeaderEntity.getBillId());
		if (Objects.nonNull(existingBill)) {
			billHeaderEntity.setBillHeaderId(existingBill.getBillHeaderId());
			billDetailDao.deleteItemsForExistingBill(existingBill.getItems());
			billHeaderDao.insertBill(billHeaderEntity);
			return billHeaderEntity;
		} else {
			billHeaderEntity.setBillId("SORNAMBIGAI_" + (currentSequence + 1));
			billHeaderDao.insertBill(billHeaderEntity);
			billSequenceDao.incrementSequence();
			return billHeaderEntity;
		}

	}

	@Override
	public boolean deleteBill(String billId) {
		BillHeaderEntity billHeaderEntity = billHeaderDao.getBillByBillId(billId);
		if (Objects.nonNull(billHeaderEntity)) {
			return billHeaderDao.deleteBill(billHeaderEntity);
		} else {
			return false;
		}
	}

	@Override
	public BillCalculationResponseModel calculateBillForSingleItem(BillCalculationRequestModel requestModel) {
		BigDecimal wastage = BigDecimal.ZERO;
		BillDetailEntity billDetailEntity = requestModel.getItemDetails();
		ItemCategoryMstEntity itemCategory = itemCategoryDao.getByItemCategoryId(billDetailEntity.getItemCategoryId());
		switch (itemCategory.getBillCalculationType()) {
		case STANDARD_SILVER:
		case STANDARD_GOLD:
			wastage = billDetailEntity.getWastage().multiply(BigDecimal.valueOf(Math.pow(10, -3.0)));
			break;
		default:
			break;
		}
		BigDecimal gramsPurchased = billDetailEntity.getGramsPurchased();
		BigDecimal totalGramsPurchased = gramsPurchased.add(wastage);
		BigDecimal totalAmountForSinglePiece = BigDecimal.ZERO;
		if (itemCategory.getBillCalculationType().getBillCalculationType() == BillCalculationType.KDM_GOLD_916
				.getBillCalculationType()) {
			BigDecimal priceWithPercentAdded = billDetailEntity.getPricePerGramOnPurchase()
					.add(billDetailEntity.getPricePerGramOnPurchase().multiply(BigDecimal.valueOf(0.14)));
			totalAmountForSinglePiece = totalGramsPurchased.multiply(priceWithPercentAdded)
					.add(billDetailEntity.getMakingCharge());
		} else {
			totalAmountForSinglePiece = totalGramsPurchased.multiply(billDetailEntity.getPricePerGramOnPurchase())
					.add(billDetailEntity.getMakingCharge());
		}
		BigDecimal totalAmount = totalAmountForSinglePiece.multiply(BigDecimal.valueOf(billDetailEntity.getQuantity()));
		billDetailEntity.setItemAmount(totalAmount);
		return BillCalculationResponseModel.builder()
				.totalAmount(requestModel.getBillHeader().getTotalBillAmount().add(totalAmount))
				.itemDetails(billDetailEntity).build();
	}

	@Override
	public List<BillHeaderEntity> searchBills(String keyword, long offset) {
		return billHeaderDao.searchBills(keyword, offset);
	}

	@Override
	public FileResponseModel closeAndPrintBill(BillHeaderEntity billRequestModel) {
		BillHeaderEntity billHeaderModel = this.insertBill(billRequestModel);
		Map<String, String> itemCategoryNameMap = itemCategoryDao.getItemCategories().stream().collect(Collectors.toMap(
				itemCategory -> itemCategory.getItemCategoryId(), itemCategory -> itemCategory.getItemCategoryName()));
		FileResponseModel fileModel = reportService.exportBill(billHeaderModel, itemCategoryNameMap);
		if (fileModel.isResult()) {
			return fileModel.toBuilder().billId(billHeaderModel.getBillId()).build();
		} else {
			return fileModel.toBuilder().failMessage("Bill Closed. Error while Exporting Bill")
					.billId(billHeaderModel.getBillId()).build();
		}
	}

	@Override
	public FileResponseModel printBill(String billId) {
		BillHeaderEntity billHeaderModel = billHeaderDao.getBillByBillId(billId);
		Map<String, String> itemCategoryNameMap = itemCategoryDao.getItemCategories().stream().collect(Collectors.toMap(
				itemCategory -> itemCategory.getItemCategoryId(), itemCategory -> itemCategory.getItemCategoryName()));
		if (Objects.nonNull(billHeaderModel)) {
			FileResponseModel fileModel = reportService.exportBill(billHeaderModel, itemCategoryNameMap);
			if (fileModel.isResult()) {
				return fileModel.toBuilder().billId(billHeaderModel.getBillId()).build();
			} else {
				return fileModel.toBuilder().failMessage("Export Failed. Sorry For the Inconvenience.")
						.billId(billHeaderModel.getBillId()).build();
			}
		}
		return FileResponseModel.builder().result(false).failMessage("Bill Not Found. Enter Correct Bill Id.").build();
	}

}
