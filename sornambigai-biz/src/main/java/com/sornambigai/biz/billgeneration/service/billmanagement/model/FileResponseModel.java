package com.sornambigai.biz.billgeneration.service.billmanagement.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class FileResponseModel {

	private String fileData;
	private String fileName;
	private boolean result;
	private String failMessage;
	private String billId;
}
