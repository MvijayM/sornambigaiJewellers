package com.sornambigai.biz.billgeneration.service.billmanagement.model;

import com.sornambigai.entity.BillDetailEntity;
import com.sornambigai.entity.BillHeaderEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Builder
public class BillCalculationRequestModel {

	private BillHeaderEntity billHeader;
	private BillDetailEntity itemDetails;
}
