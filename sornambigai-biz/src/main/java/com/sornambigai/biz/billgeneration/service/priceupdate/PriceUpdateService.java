package com.sornambigai.biz.billgeneration.service.priceupdate;

import java.util.List;
import java.util.Map;

import com.sornambigai.entity.PriceMstEntity;

public interface PriceUpdateService {

	public boolean updatePrice(Map<String, Object> requestMap);

	public List<PriceMstEntity> getPrices();

}
