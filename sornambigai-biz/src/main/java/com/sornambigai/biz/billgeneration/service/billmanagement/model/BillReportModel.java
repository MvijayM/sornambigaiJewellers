package com.sornambigai.biz.billgeneration.service.billmanagement.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import com.sornambigai.entity.BillDetailEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BillReportModel {

	private long serialNumber;

	private String itemName;

	private String itemCategory;

	private BigDecimal gramsPurchased;

	private BigDecimal wastage;

	private BigDecimal pricePerGram;

	private BigDecimal makingCharge;
	private BigDecimal itemAmount;
	private Long noOfPieces;

	public static List<BillReportModel> toReportModel(List<BillDetailEntity> detailEntityList,
			Map<String, String> itemCategoryNameMap) {
		AtomicLong sNo = new AtomicLong();
		return detailEntityList.stream().map(detailEntity -> {
			BillReportModel reportModel = new BillReportModel();
			reportModel.setSerialNumber(sNo.incrementAndGet());
			reportModel.setItemName(detailEntity.getItemName());
			reportModel.setItemCategory(itemCategoryNameMap.get(detailEntity.getItemCategoryId()));
			reportModel.setGramsPurchased(detailEntity.getGramsPurchased());
			reportModel.setWastage(detailEntity.getWastage());
			reportModel.setItemAmount(detailEntity.getItemAmount());
			reportModel.setPricePerGram(detailEntity.getPricePerGramOnPurchase());
			reportModel.setMakingCharge(detailEntity.getMakingCharge());
			reportModel.setNoOfPieces(detailEntity.getQuantity());
			return reportModel;

		}).collect(Collectors.toList());
	}

}
