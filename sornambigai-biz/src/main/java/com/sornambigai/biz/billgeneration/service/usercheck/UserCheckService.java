package com.sornambigai.biz.billgeneration.service.usercheck;

import java.util.Map;

import com.sornambigai.biz.billgeneration.service.usercheck.model.ChangePasswordResponseModel;
import com.sornambigai.entity.UserEntity;

public interface UserCheckService {
	public boolean check(Map<String, Object> requestMap);

	public boolean addUser(UserEntity userEntity);

	public boolean checkIFAdminExist();

	public ChangePasswordResponseModel changePassword(Map<String, Object> requestModel);
}
