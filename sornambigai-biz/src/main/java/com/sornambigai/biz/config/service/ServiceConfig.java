package com.sornambigai.biz.config.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sornambigai.biz.billgeneration.service.billmanagement.BillManagementService;
import com.sornambigai.biz.billgeneration.service.billmanagement.BillManagementServiceImpl;
import com.sornambigai.biz.billgeneration.service.billmanagement.report.ReportService;
import com.sornambigai.biz.billgeneration.service.billmanagement.report.ReportServiceImpl;
import com.sornambigai.biz.billgeneration.service.items.ItemsService;
import com.sornambigai.biz.billgeneration.service.items.ItemsServiceImpl;
import com.sornambigai.biz.billgeneration.service.priceupdate.PriceUpdateService;
import com.sornambigai.biz.billgeneration.service.priceupdate.PriceUpdateServiceImpl;
import com.sornambigai.biz.billgeneration.service.usercheck.UserCheckService;
import com.sornambigai.biz.billgeneration.service.usercheck.UserCheckServiceImpl;
import com.sornambigai.dao.BillDetailDao;
import com.sornambigai.dao.BillHeaderDao;
import com.sornambigai.dao.BillSequenceDao;
import com.sornambigai.dao.ItemCategoryMstDao;
import com.sornambigai.dao.ItemsDao;
import com.sornambigai.dao.PriceMstDao;
import com.sornambigai.dao.UserDao;

@Configuration
public class ServiceConfig {

	@Bean
	@Autowired
	public UserCheckService userCheckService(UserDao userDao) {
		return new UserCheckServiceImpl(userDao);
	}

	@Bean
	@Autowired
	public ItemsService itemsService(ItemsDao itemsDao, ItemCategoryMstDao itemCategoryMstDao) {
		return new ItemsServiceImpl(itemsDao, itemCategoryMstDao);
	}

	@Bean
	@Autowired
	public PriceUpdateService priceUpdateService(PriceMstDao priceMstDao) {
		return new PriceUpdateServiceImpl(priceMstDao);
	}

	@Bean
	@Autowired
	public ReportService reportService() {
		return new ReportServiceImpl();
	}

	@Bean
	@Autowired
	public BillManagementService billManagementService(BillHeaderDao BillHeaderDao, BillDetailDao billDetailDao,
			ItemCategoryMstDao itemCategoryDao, BillSequenceDao billSequenceDao, ReportService reportService,
			PriceUpdateService priceUpdateService) {
		return new BillManagementServiceImpl(BillHeaderDao, billDetailDao, itemCategoryDao, billSequenceDao,
				reportService, priceUpdateService);
	}
}
